Boucles


Exo 1

<?php

$i = 0;

while($i < 10)
{
echo $i;
$i++;
}

?>



Exo 2

<?php

$a = 0;
$b = rand(1,100);

while($a<20)
{
echo $a*$b."\n";
$a++;
}

?>




Exo 3

<?php

$a = 100;
$b = rand(1,100);

while ($a>10)
{
  echo $a*$b."\n";
  $a--;
}


?>





Exo 4

<?php

$i = 1;

while ($i<10)
{
  echo "$i\n";
  $i = $i + $i/2;
}


?>



Exo 5

<?php

$i=1;

while ($i<16)
{
  echo "On y arrive presque...\n";
  $i++;
}

?>



Exo 6

<?php

$i=20;

while ($i>0)
{
  echo "C'est presque bon...\n";
  $i--;
}

?>



Exo 7

<?php

$i=1;

while ($i<=100)
{
  echo "On tient le bon bout...\n";
  $i = $i + 14;
}

?>




Exo 8

<?php

$i=200;

while ($i>=0)
{
  echo "Enfin!!!\n";
  $i = $i - 13;
}

?>













